# recipe app api proxy

NGINX proxy app for our recipe app api

## Usage

### Environment Variables

* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - Hostname of app to forward request to (defult: `app`)
* `APP_PORT` - Port of the app to forward request to (deault: `9000`)
